﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;

namespace SleepStageEstimation{
  public static class Program{
    /// <summary>
    /// Main
    /// </summary>
    /// <param name="args">コマンドライン引数(--helpでヘルプ表示)</param>
    public static void Main(string[] args){
      string rf_path=null,alice_file=null,tanita_file=null,emfit_file=null,output_file=null;

      // 引数がない場合やVSから実行した時
      if(args.Length==0){
        rf_path="..\\..\\lab9"; // RFモデルの保存先ディレクトリ
        alice_file=@"D:\Dropbox\realtimesleepstageestimator_\140930_Hayashi\ALICE\bbb.csv"; // ALICEのファイル
        tanita_file=@"D:\Dropbox\realtimesleepstageestimator_\140930_Hayashi\TANITA\1409302207048801_signal.csv"; // TANITAのファイル
        emfit_file=@"D:\Dropbox\realtimesleepstageestimator_\140930_Hayashi\EMFit\1409302207293098.csv"; // EMFITのファイル
        output_file="result.csv"; // ファイルの出力先
      }else // コマンドライン引数あり 
        Parser.Default.ParseArguments<Options>(args).WithParsed(o=>{
          rf_path=o.RandomForest; // RFモデルの保存先ディレクトリ
          alice_file=o.Alice; // ALICEのファイル
          tanita_file=o.Tanita; // TANITAのファイル
          emfit_file=o.Emfit; // EMFITのファイル
          output_file=o.Outout; // ファイルの出力先
        }).WithNotParsed(o=>Environment.Exit(0));


      // RFの学習をしたい時ファイル名を配列にして渡すと複数のファイルを読み込む
      string[] tanitas=new string[]{@"D:\Dropbox\realtimesleepstageestimator_\140930_Hayashi\TANITA\1409302207048801_signal.csv"};
      string[] alices=new string[]{@"D:\Dropbox\realtimesleepstageestimator_\140930_Hayashi\ALICE\bbb.csv"};
      //KomineMethod.GenerateRandomForestModel(tanitas,alices,rf_path);

      // 睡眠段階推定をしたい時
      var records=new Records();
      try{
        if(alice_file!=null)
          FileReader.ReadAliceFile(alice_file,ref records);
        if(tanita_file!=null)
          FileReader.ReadTanitaRawFile(tanita_file,ref records);
        if(emfit_file!=null)
          FileReader.ReadEmfitFile(emfit_file,ref records);
        if(emfit_file!=null)
          HaradaMethod.EstimateSleepStage(records,new HaradaMethod.Parameter()); 
        if(rf_path!=null&&tanita_file!=null)
          KomineMethod.EstimateSleepStage(records,rf_path,new KomineMethod.Parameter());
        if(emfit_file!=null&&rf_path!=null&&tanita_file!=null)
          UeharaMethod.EstimateSleepStage(records,new UeharaMethod.Parameter());
        records.Trim();
        if(alice_file!=null)
          records.CalcAccuracy();
        if(output_file!=null)
          records.SaveToFile(output_file);
      }catch(Exception){
        Console.WriteLine("エラー: コマンドライン引数が正しくないかデータが不正です。コマンドライン引数は--helpで確認可能です。");
        Environment.Exit(0);
      }
    }

    /// <summary>
    /// コマンドライン引数の処理用クラス
    /// </summary>
    private class Options{
      [Option('a',"alice",HelpText="ALICE(PSG)のファイルパス(ファイル先頭にキャプションが必要)")]
      public string Alice{get;set;}=null;
      [Option('e',"emfit",HelpText="EMFITのファイルパス")]
      public string Emfit{get;set;}=null;
      [Option('t',"tanita",HelpText="TANITA(Raw)のファイルパス")]
      public string Tanita{get;set;}=null;
      [Option('r',"rf",HelpText="Random Forestsのモデルのディレクトリパス")]
      public string RandomForest{get;set;}=null;
      [Option('o',"output",HelpText="結果の出力先ファイルパス")]
      public string Outout{get;set;}=null;
    }
  }
}
