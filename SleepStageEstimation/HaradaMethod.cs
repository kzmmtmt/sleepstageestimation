﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using MathNet.Numerics.Statistics;

namespace SleepStageEstimation{
  /// <summary>
  /// 原田手法
  /// </summary>
  public static class HaradaMethod{
    public const string NAME="Harada";

    /// <summary>
    /// 睡眠段階の推定
    /// </summary>
    /// <param name="data">レコード</param>
    /// <param name="param">パラメーター群</param>
    public static void EstimateSleepStage(Records records,Parameter param){
      Console.WriteLine("原田手法で推定しています");
      if(records.Contents.Where(x=>x.HR!=null).ToArray().Length==0){
        Console.WriteLine("エラー: EMFITのファイルが読み込まれていません");
        Environment.Exit(1);
      }
      var data=CheckHR(param.Trimming?Trim(records.Contents.ToList()):records.Contents.ToArray());
      var cofficient=CalcCofficient(data,param);
      var hr=CalcEstimatedHR(data,cofficient,param);
      bool[] rem_flag;
      int?[] wake_flag;
      rem_flag=param.RemCorrect?RemDetection(data,hr):Enumerable.Repeat(false,data.Length).ToArray();
      var ss=Descrization(hr,rem_flag,data,param);
      wake_flag=param.WakeCorrect?WakeDetection(data,ss,rem_flag,60):Enumerable.Repeat((int?)null,data.Length).ToArray();

      // 出力
      records.AddMethodKey(NAME);
      var start=records.FindIndex(data.First().Time);
      Parallel.For(0,data.Length,i=>{
        if(wake_flag[i]!=null)
          records[i+start][NAME]=wake_flag[i];
        else
          records[i+start][NAME]=ss[i];
      });
    }

    /// <summary>
    /// HRが10以下のときに前の値で埋める(松本追加)
    /// </summary>
    /// <param name="r">データ</param>
    /// <returns>HRが10以下の時のデータを前の値で埋めたデータ</returns>
    private static Record[] CheckHR(Record[] r){
      Console.WriteLine("データの検証をしています");
      var records=r.Where(x=>x.HR!=null).ToArray();
      var checked_data=new Record[records.Length];
      checked_data[0]=records[0].Clone();
      for(int i=1;i<records.Length;i++){
        checked_data[i]=records[i].Clone();
        if(records[i].HR<=10&&checked_data[i-1].HR>10)
          checked_data[i].HR=checked_data[i-1].HR;
      }
      var last_index=0;
      for(int i=records.Length-1;i>=0;i--) {
        if(records[i].HR>10){
          last_index=i;
          break;
        }
      }
      for(int i=last_index;i<records.Length;i++)
        checked_data[i].HR=records[i].HR;
      return checked_data;
    }

    /// <summary>
    /// 起きているデータを削除(寝始めから起床までをトリミング)
    /// </summary>
    /// <param name="data">データ</param>
    /// <param name="interval">何秒までの途中起床を許容するか [600]</param>
    /// <param name="bm_threshold">? [20]</param>
    /// <param name="bm_time">? [120]</param>
    /// <param name="trimming_before">トリミングするときに残す時間(秒) [0]</param>
    public static Record[] Trim(List<Record> r,int interval=600,int bm_threshold=20,int bm_time=120,int trimming_before=0){
      Console.WriteLine("トリミングしています");
      var max_bm_with_no_hr=r.Where(i=>i.HR<10).Select(i=>i.BM).Min(); // 人が居ない時のセンサーの値(HRが10より小さい時のBMの最小値)
      max_bm_with_no_hr=max_bm_with_no_hr==null?0:max_bm_with_no_hr;
      if(r.Where(i=>i.HR!=null).Count()==0){
        Console.WriteLine("HRが空なのでデータのトリミングできません");
        return r.ToArray();
      }
      var data=r.Where(i=>i.HR!=null).ToList();
      bool[] sleeping1=new bool[data.Count];
      byte[] sleeping2=new byte[data.Count];
      int[] hr=new int[bm_time];
      int[] bm=new int[bm_time];
      int tail=0;
      for(int i=0;i<data.Count;i++){
        hr[tail]=(int)data[i].HR;
        bm[tail]=(int)data[i].BM;
        tail=(tail+1)%bm_time;
        if(i<bm_time){
          sleeping1[i]=false;
          sleeping2[i]=0;
          continue;
        }
        if(sleeping2[i-1]==2&&data[i].HR>=10){
          sleeping2[i]=2;
          continue;
        }
        if(data[i].HR<10||(sleeping2[i-1]==0&&data[i].BM<max_bm_with_no_hr))
          sleeping2[i]=0;
        else{
          sleeping2[i]=2;
          for(int j=0;j<bm_time;j++)
            if(bm[j]>bm_threshold){
              sleeping2[i]=1;
              break;
            }
        }

        sleeping1[i]=true;
        for(int j=0;j<bm_time;j++){
          if(hr[j]<10){
            // ベッドに居ない
            sleeping1[i]=false;
            break;
          }
          if(!sleeping1[i-1])
            if(bm[j]>bm_threshold){
              // ベッドの上にいるけど起きている
              sleeping1[i]=false;
              break;
            }
        }
      }

      int sleep_continue=0,not_sleep_continue=0,start_point=0,stop_point=0,tmp_start_point=0,tmp_stop_point=-1;
      for(int i=0;i<data.Count;i++){
        if(sleeping2[i]==2||(sleep_continue!=0&&sleeping2[i]==1)){
          if(sleep_continue==0)
            start_point=i;
          sleep_continue++;
          not_sleep_continue=0;
        }else{
          if(not_sleep_continue==interval&&sleep_continue!=0){
            sleep_continue=0;
            stop_point=i-interval;
            if(tmp_stop_point-tmp_start_point<stop_point-start_point){
              tmp_start_point=start_point;
              tmp_stop_point=stop_point;
            }
          }
          not_sleep_continue++;
        }
      }
      if(tmp_stop_point==-1){
        tmp_start_point=start_point;
        tmp_stop_point=data.Count;
      }
      if(trimming_before>0){
        tmp_start_point-=trimming_before;
        if(tmp_start_point<0)
          tmp_start_point=0;
      }
      var trimmed=data.GetRange(tmp_start_point,tmp_stop_point-tmp_start_point).ToArray();
      Console.WriteLine($"{trimmed.First().Time.ToLongTimeString()}-{trimmed.Last().Time.ToLongTimeString()}が切り取られました({Utilities.SecToString(data.Count-trimmed.Length)}削除されました)");
      return trimmed;;
    }


    /// <summary>
    /// 係数の計算
    /// </summary>
    /// <param name="data">データ</param>
    /// <param name="param">パラメーター</param>
    /// <returns>係数</returns>
    private static double[] CalcCofficient(Record[] data,Parameter param){
      Console.WriteLine("係数の計算をしています");
      var sum_y=Enumerable.Repeat(0.0,param.N).ToArray();
      var matrix=new double[param.N,param.N];
      Parallel.For(0,param.N,i=>{
        Parallel.For(0,param.N,j=>{
          matrix[i,j]=0;
        });
      });
      foreach(var d in data){
        var t=(int)(d.Time-data[0].Time).TotalSeconds;
        Parallel.For(0,param.N,i=>{
          sum_y[i]+=(double)d.HR*SinCos(t,i,param);
          for(int j=1;j<param.N;j++){
            matrix[i,j]+=SinCos(t,i,param)*SinCos(t,j,param);
            matrix[j,i]=matrix[i,j];
          }
        });
      }
      return Matrix.Dot(Matrix.Inverse(matrix),sum_y);
    }

    /// <summary>
    /// 心拍の中周波の推定
    /// </summary>
    /// <param name="data">データ</param>
    /// <param name="cofficient">係数</param>
    /// <param name="param">パラメーター</param>
    /// <returns>推定された中周波</returns>
    private static double[] CalcEstimatedHR(Record[] data,double[] cofficient,Parameter param){
      Console.WriteLine("中周波の推定を行っています");
      const int ADDITIONAL=300;
      double[] estimated_hr=new double[Math.Max(data.Length+1,param.MaxCycle)+ADDITIONAL];
      var min_index=Math.Max(0,(int)(Math.Floor(param.MaxCycle/Math.Pow(2.0,14.0))-1)*2);
      var max_index=(int)((Math.Floor(param.MaxCycle/Math.Pow(2.0,14.0)*12.0)+1)*2);
      Parallel.For(0,estimated_hr.Length,i=>{
        estimated_hr[i]=0.0;
        if(cofficient.Length-1<max_index)
          max_index=cofficient.Length-1;
        for(int j=min_index;j<max_index;j++)
          estimated_hr[i]+=cofficient[j]*SinCos(i,j,param);
        estimated_hr[i]+=cofficient.Last();
      });
      return estimated_hr;
    }

    /// <summary>
    /// 離散化して睡眠段階を推定
    /// </summary>
    /// <param name="estimated_hr">補正中周波</param>
    /// <param name="rem_flag">REMフラグ</param>
    /// <returns>睡眠段階</returns>
    private static int[] Descrization(double[] estimated_hr,bool[] rem_flag,Record[] data,Parameter param){
      Console.WriteLine("離散化しています");
      var stdev=estimated_hr.ToList().GetRange(0,param.MaxCycle).StandardDeviation();
      var avr=estimated_hr.ToList().GetRange(0,param.MaxCycle).Average();
      var sleep_stage=new int[data.Length];
      Parallel.For(0,sleep_stage.Length,i=>{
        if(rem_flag[i])
          sleep_stage[i]=4;
        else{
          var ss=(int)Math.Ceiling((estimated_hr[i]-avr)/stdev+2.0);
          sleep_stage[i]=ss<0?0:ss>5?5:ss;
        }
      });
      return sleep_stage;
    }

    /// <summary>
    /// sin/cos関数
    /// </summary>
    /// <param name="t">時刻</param>
    /// <param name="index">index</param>
    /// <param name="param">パラメーター</param>
    /// <returns></returns>
    private static double SinCos(int t,int index,Parameter param){
      if(index==param.N-1)
        return 1;
      else if(index%2==0)
        return Math.Sin(2.0*Math.PI*t*((index/2.0+1)/(double)param.MaxCycle));
      return Math.Cos(2.0*Math.PI*t*(((index-1)/2.0+1)/(double)param.MaxCycle));
    }

    /// <summary>
    /// REM補正
    /// </summary>
    /// <param name="data">データ</param>
    /// <param name="estimated_hr">補正中周波</param>
    /// <returns>データに対応したREMのフラグ</returns>
    private static bool[] RemDetection(Record[] data,double[] estimated_hr){
      Console.WriteLine("REM補正を掛けています");
      const int CURRENT_TIME_WINDOW=5,PREV_TIME_WINDOW=5;
      const double REM_START_THRESHOLD=0.04;
      double prev_diff=0.0;
      var flag=Enumerable.Repeat(false,data.Length).ToArray();
      var current_rem_flag=false;
      for(int t=(CURRENT_TIME_WINDOW+PREV_TIME_WINDOW)*60;t<=(int)(data.Length/60)*60;t+=60){
        var max_bm=data.ToList().GetRange(t-59,60).Select(x=>x.BM).Max();
        var current_median=data.ToList().GetRange(t-60*CURRENT_TIME_WINDOW+1,60*CURRENT_TIME_WINDOW).Select(x=>(double)x.HR).Median();
        var prev_median=data.ToList().GetRange(t-60*(CURRENT_TIME_WINDOW+PREV_TIME_WINDOW)+1,60*PREV_TIME_WINDOW).Select(x=>(double)x.HR).Median();
        if(!current_rem_flag){
          if(max_bm>100)
            continue;
          var tmp=(current_median-prev_median)/prev_median;
          if(tmp>=REM_START_THRESHOLD){
            prev_diff=tmp;
            for(int i=t-59;i<=t;i++)
              flag[i]=true;
            current_rem_flag=true;
            continue;
          }else
            continue;
        }else{
          var tmp=(current_median-prev_median)/prev_median;
          if(prev_diff<0.0&&prev_diff<tmp){
            current_rem_flag=false;
            continue;
          }else{
            prev_diff=tmp;
            for(int i=t-59;i<=t;i++)
              flag[i]=true;
            continue;
          }
        }
      }
      return flag;
    }
    
    /// <summary>
    /// Wake補正
    /// </summary>
    /// <param name="data">データ</param>
    /// <param name="ss">睡眠段階</param>
    /// <param name="rem_flag">REMのフラグ</param>
    /// <param name="time_window">ウィンドウサイズ</param>
    /// <returns>データに対応したREMのフラグ</returns>
    private static int?[] WakeDetection(Record[] data,int[] ss,bool[] rem_flag,int time_window){
      Console.WriteLine("Wake補正を掛けています");
      const int INTERVAL=10;
      const int SKIP_INTERVAL=0;
      int?[] flag=Enumerable.Repeat((int?)null,data.Length).ToArray();
      Parallel.For(0,data.Length/time_window,t=>{
        var std_bm=data.ToList().GetRange(t*time_window,time_window).Select(x=>(double)x.BM).StandardDeviation();
        var avr_bm=data.ToList().GetRange(t*time_window,time_window).Select(x=>(double)x.BM).Average();
        if(std_bm/data.Select(x=>(double)x.BM).Average()>1)
          for(int i=t*time_window;i<t*time_window+time_window;i++)
            flag[i]=5;
      });
      Parallel.For(INTERVAL+SKIP_INTERVAL/60,data.Length/60,t=>{
        var avr_bm=data.ToList().GetRange((t-1)*60,60).Select(x=>(double)x.BM).Average();
        var q3=data.ToList().GetRange((t*60)-(60*INTERVAL+SKIP_INTERVAL),60*INTERVAL+SKIP_INTERVAL).Select(x=>(double)x.BM).Quantile(0.75);
        var start=(t*60)-(60*20+SKIP_INTERVAL);
        var not_w1_60=data.ToList().GetRange(start<0?0:start,start<0?t*60:60*20+SKIP_INTERVAL).Select(x=>(double)x.BM).Quantile(0.6);
        // Wake-NR1判定
        if(avr_bm>q3)
          for(int i=t*60-time_window;i<t*60;i++){
            if(ss[i]!=4&&ss[i]!=5&&flag[i]!=5)
              flag[i]=3;
            if(ss[i]==4&!rem_flag[i]&&flag[i]!=5)
              flag[i]=3;
          }
        // NotWake-NR1判定
        else if(avr_bm<not_w1_60)
          for(int i=t*60-time_window;i<t*60;i++)
            if(ss[i]!=4&&ss[i]>=3&&flag[i]!=5)
              flag[i]=2;          
      });
      return flag;
    }


    /// <summary>
    /// 原田手法のパラメーター
    /// </summary>
    public class Parameter{
      /// <summary>
      /// 最大周期(sec) [16384]
      /// </summary>
      public int MaxCycle{private set;get;}
      /// <summary>
      /// 係数の数 [(int)Math.Ceiling(MaxCycle/60.0/22.0)*2+1]
      /// </summary>
      public int N{set;get;}
      /// <summary>
      /// REM補正 [true]
      /// </summary>
      public bool RemCorrect{set;get;}=true;
      /// <summary>
      /// Wake補正 [true]
      /// </summary>
      public bool WakeCorrect{set;get;}=true;
      /// <summary>
      /// トリミング [true]
      /// </summary>
      public bool Trimming{set;get;}=true; // トリミングするか

      /// <summary>
      /// コンストラクタ
      /// </summary>
      /// <param name="max_cycle">最大周期(sec) [16384]</param>
      public Parameter(int max_cycle=16384){
        MaxCycle=max_cycle;
        N=(int)Math.Ceiling(MaxCycle/60.0/22.0)*2+1;
      }
    }

  }
}
