﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SleepStageEstimation{
  /// <summary>
  /// 1日分のデータ
  /// </summary>
  public class Records{
    private List<Record> data;
    public Record this[int index]{
      get{return data[index];}
      set{data[index]=value;}
    }
    public bool HasPSG{get;set;}=false;
    public int Count{get{return data.Count;}}
    public List<Record> Contents{get{return data;}}
    private List<string> keys; // 手法名のキー
    private string accuracy=null;

    public Records(){
      data=new List<Record>();
      keys=new List<string>();
    }

    /// <summary>
    /// 手法名のキーの登録
    /// </summary>
    /// <param name="key">手法名</param>
    public void AddMethodKey(string key){
      keys.Add(key);
    }

    /// <summary>
    /// 手法名のキーが存在するかを調べる
    /// </summary>
    /// <param name="key">手法名</param>
    /// <returns></returns>
    public bool HasMethodKey(string key){
      return keys.Contains(key);
    }

    /// <summary>
    /// レコード自体の欠損と重複の修復を行う。
    /// 前後で時間が重複している場合、最初のレコードのみ残して削除する。
    /// 時間が欠損している場合は前の時間の状態をコピーする。
    /// </summary>
    public void ModifyLossAndOverlapping(){
      if(data.Count<2)
        return;
      for(int i=1;i<Count;i++){
        var t=(int)((data[i].Time-data[i-1].Time).TotalSeconds); // 1つ前のレコードとの差
        if(t<=0){ // 重複しているか負(データが逆転している)ならば削除
          data.RemoveAt(i);
          i--;
        }else if(t>1){ // 間が欠損しているならば前の値で埋める
          for(int j=t-1;j>0;j--)
            data.Insert(i,new Record(data[i-1].Time.AddSeconds(j)));
          i+=t-1;
        }
      }
      // データの補間
      for(int i=1;i<data.Count;i++){
        if(data[i].HR==null&&data[i-1].HR!=null)
          data[i].HR=data[i-1].HR;
        if(data[i].BM==null&&data[i-1].BM!=null)
          data[i].BM=data[i-1].BM;
        if(data[i].SensorValues.Count==0&&data[i-1].SensorValues.Count>0)
          data[i].SensorValues=new List<int>(data[i-1].SensorValues);
        if(data[i].Spectrum==null&&data[i-1].Spectrum!=null)
          data[i].Spectrum=Utilities.DeepArrayCopy(data[i-1].Spectrum);
        if(data[i].PSG==null&&data[i-1].PSG!=null)
          data[i].PSG=data[i-1].PSG;
      }
    }

    /// <summary>
    /// ある時刻のレコードのインデックスを返すメソッド
    /// </summary>
    /// <param name="dt">時刻</param>
    /// <returns>インデックス</returns>
    public int FindIndex(DateTime dt){
      return data.IndexOf(data.Find(x=>x.Time==dt));
    }

    /// <summary>
    /// 睡眠段階推定結果に合わせてデータをトリミング
    /// </summary>
    public void Trim(){
      var start=new List<int>();
      var end=new List<int>();
      if(HasPSG){
        for(int i=0;i<Count;i++)
          if(this[i].PSG!=null){
            start.Add(i);
            break;
          }
        for(int i=Count-1;i>=0;i--)
          if(this[i].PSG!=null){
            end.Add(i);
            break;
        }
      }
      Parallel.For(0,keys.Count,k=>{
        for(int i=0;i<Count;i++)
          if(this[i][keys[k]]!=null){
            start.Add(i);
            break;
          }
        for(int i=Count-1;i>=0;i--)
          if(this[i][keys[k]]!=null){
            end.Add(i);
            break;
        }
      });
      var s=start.Count==0?0:start.Min();
      var e=end.Count==0?Count-1:end.Max();
      data.RemoveRange(e,Count-e);
      data.RemoveRange(0,s);
    }

    /// <summary>
    /// ファイルに出力
    /// </summary>
    /// <param name="filename">ファイル名(.csv)</param>
    public void SaveToFile(string filename){
      try{
        using(var sr=new StreamWriter(filename)){
          if(accuracy!=null)
            sr.WriteLine(accuracy);
          sr.Write(ToString());
        }
        Console.WriteLine($"結果を{filename}に出力しました");
      }catch(Exception e){
        Console.WriteLine($"エラー: ファイルの読み込みに失敗しました\n{e.Message}");
        Environment.Exit(1);
      }
    }

    /// <summary>
    /// 完全一致率の計算
    /// </summary>
    public void CalcAccuracy(){
      Console.WriteLine("推定精度を計算しています");
      if(!HasPSG){
        Console.WriteLine("警告: PSGデータが読み込まれていないため、精度を計算できません");
        return;
      }
      string str="[Accuracy] ";
      for(int i=0;i<keys.Count;i++){
        var tmp=data.Where(x=>x.PSG!=null&&x[keys[i]]!=null);
        var p=100.0*tmp.Where(x=>x.PSG==x[keys[i]]).ToArray().Length/tmp.ToArray().Length;
        str+=$"{keys[i]}:{Math.Round(p,2)}% ";
      }
      accuracy=str;
      Console.WriteLine(str);
    }

    /// <summary>
    /// 全てのレコードを文字列に変換
    /// </summary>
    /// <returns>全レコードデータ</returns>
    public override string ToString(){
      if(Count>0){
        string tmp=HasPSG?"PSG,":"";
        return $"{Record.HEADER}{tmp}{string.Join(",",keys)}\n{string.Join("\n",data.Select(d=>d.ToString(HasPSG,keys.ToArray())))}\n";
      }
      return "";
    }

  }
}
