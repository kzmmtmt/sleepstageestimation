﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math.Transforms;
using System.Numerics;
using SharpLearning.RandomForest.Learners;
using SharpLearning.RandomForest.Models;
using SharpLearning.Containers.Matrices;
using System.IO;


namespace SleepStageEstimation{
  /// <summary>
  /// 小峯手法
  /// </summary>
  public static class KomineMethod{
    public const string NAME="Komine";
    private static readonly string[] RF_FILE={"rem.xml","nr2.xml","nr1.xml"}; // REM/NR2/NR1分類器の順
    private const char PATH_SEPARATOR='\\'; // パスの区切り

    /// <summary>
    /// 睡眠段階の推定
    /// </summary>
    /// <param name="data">TANITAのRawデータ</param>
    /// <param name="rf_path">Random Forestsの学習ファイルが保存されているディレクトリのパス</param>
    /// <param name="param">パラメーター群</param>
    public static void EstimateSleepStage(Records data,string rf_path,Parameter param){
      Console.WriteLine("小峯手法で推定しています");
      var records=data.Contents.Where(x=>x.SensorValues.Count!=0).ToArray();
      if(records.Length<64){
        Console.WriteLine("エラー: Rawデータが読み込まれていないか64個以下しかありません");
        Environment.Exit(1);
      }
      GenerateSpectrumData(records);
      var target=records.Where(x=>x.Spectrum!=null).ToArray();
      var ss=Classify(target,rf_path);

       // 出力
      data.AddMethodKey(NAME);
      var start=data.FindIndex(target.First().Time);
      Parallel.For(0,target.Length,i=>{
        records[i+start][NAME]=ss[i];
      });

      // 原田手法のトリミング
      if(param.UseHaradaTrimmingMethod)
        if(data.HasMethodKey(HaradaMethod.NAME)){
          Parallel.For(0,data.Count,i=>{
            if(data[i][HaradaMethod.NAME]==null)
              data[i][NAME]=null;
          });
        }else
          Console.WriteLine("警告: 原田手法の推定がされていないのでトリミングをスキップします");
      // 原田手法のWakeをパクる
      if(param.UseHaradaTrimmingMethod)
        if(data.HasMethodKey(HaradaMethod.NAME)){
          Parallel.For(0,data.Count,i=>{
            if(data[i][HaradaMethod.NAME]==5)
              data[i][NAME]=5;
          });
        }else
          Console.WriteLine("警告: 原田手法の推定がされていないのでWake補正をスキップします");
    }

    /// <summary>
    /// 64秒(16Hzで1024個のデータ)からFFTを掛けてRawデータに格納
    /// </summary>
    /// <param name="data">Rawデータ</param>
    public static void GenerateSpectrumData(Record[] data){
      Console.WriteLine("パワースペクトルを計算しています");
      Parallel.For(63,data.Length,t=>{
        // 64秒前からデータを全て持ってくる
        var tmp=new List<int>();
        for(int i=0;i<64;i++)
          tmp.AddRange(data[t-i].Get16HzData());
        var c=new Complex[64*16];
        for(int i=0;i<64*16;i++)
          c[i]=new Complex((double)tmp[i],0);
        FourierTransform2.FFT(c,Accord.Math.FourierTransform.Direction.Forward);
        var f=c.Select(x=>x.Magnitude);
        // 総和が1になるようにする
        var f_sum=f.Sum();
        // これが5Hzらしい
        data[t].Spectrum=f.Select(x=>x/f_sum).ToList().GetRange(11,323).ToArray();
      });
    }

    /// <summary>
    /// Random Forestsを使って分類
    /// </summary>
    /// <param name="data">Rawデータ</param>
    /// <param name="rf_path">Random Forestsの学習ファイルが保存されているディレクトリのパス</param>
    /// <returns></returns>
    private static int?[] Classify(Record[] data,string rf_path){
      Console.WriteLine("分類を行います");
      if(rf_path.Last()!=PATH_SEPARATOR)
        rf_path+=PATH_SEPARATOR;
      var rf=new ClassificationForestModel[3]; // REM/NR2/NR1分類器の順
      try{
        Parallel.For(0,3,i=>{
          Console.WriteLine($"ランダムフォレストモデル[{rf_path}{RF_FILE[i]}]を読み込んでいます");
          rf[i]=ClassificationForestModel.Load(()=>new StreamReader(rf_path+RF_FILE[i]));
        });
      }catch(Exception e){
        Console.WriteLine($"エラー: モデルファイルを読み込めませんでした\n{e}");
        Environment.Exit(1);
      }
      var ss=new int?[data.Length];
      Parallel.For(0,ss.Length,t=>{
        if(data[t].Spectrum==null)
          ss[t]=null;
        else{
          try{
            if(rf[0].Predict(data[t].Spectrum)==1.0)
              ss[t]=4; // REM
            else if(rf[1].Predict(data[t].Spectrum)==1.0)
              ss[t]=2; // NR2
            else if(rf[2].Predict(data[t].Spectrum)==1.0)
              ss[t]=3; // NR1
            else
              ss[t]=1; // 残ったのはNR3
          }catch(Exception e){
            Console.WriteLine($"エラー: ランダムフォレストでの分類に失敗しました。モデルが正しいことを確認してください。\n{e}");
            Environment.Exit(1);
          }
        }
      });
      return ss;
    }

    /// <summary>
    /// RandomForestモデルの学習
    /// </summary>
    /// <param name="raw_files">学習に使うTANITAのRawファイルのパス(配列のインデックスはpsg_filesと揃える)</param>
    /// <param name="psg_files">学習に使うAliceのPSGファイルのパス(配列のインデックスはraw_filesと揃える)</param>
    /// <param name="output_path">出力ファイルの保存先</param>
    /// <param name="trees">木の数 [300]</param>
    public static void GenerateRandomForestModel(string[] raw_files,string[] psg_files,string output_path,int trees=300){
      if(output_path.Last()!=PATH_SEPARATOR)
        output_path+=PATH_SEPARATOR;
      if(!System.IO.Directory.Exists(output_path)){
        Console.WriteLine($"エラー: 指定されたディレクトリ({output_path})は存在しません");
        Environment.Exit(1);
      }
      if(raw_files.Length!=psg_files.Length){
        Console.WriteLine("エラー： 入力のファイル数が合いません");
        Environment.Exit(1);
      }else if(raw_files.Length==0){
        Console.WriteLine("エラー: 入力のファイルがありません");
        Environment.Exit(1);
      }
      Console.WriteLine("ランダムフォレストの学習モデルを生成します");
      var record=new List<Record>[raw_files.Length];
      Parallel.For(0,raw_files.Length,i=>{
        var records=new Records();
        FileReader.ReadTanitaRawFile(raw_files[i],ref records);
        GenerateSpectrumData(records.Contents.ToArray());
        FileReader.ReadAliceFile(psg_files[i],ref records);
        record[i]=records.Contents.Where(x=>x.Spectrum!=null&&x.PSG!=null).ToList();
      });
      var tmp=record[0];
      for(int i=1;i<raw_files.Length;i++){
        tmp=tmp.Concat(record[i]).ToList();
      }
      tmp=tmp.OrderBy(x=>Guid.NewGuid()).ToList(); // ランダムで並べ替え
      Console.WriteLine($"{raw_files.Length}個のファイルから合計{tmp.Count}個のデータを読み込みました");
      var data=new double[3][][];
      var ans=new double[3][];

      var rem=tmp.Where(x=>x.PSG==4).ToList(); // REMのデータ
      Console.WriteLine($"R: {rem.Count}");
      var nrem=tmp.Where(x=>x.PSG<4).ToList(); // NRのデータ
      Console.WriteLine($"NR: {nrem.Count}");
      var rem_nrem=rem.GetRange(0,Math.Min(rem.Count,nrem.Count)).Concat(nrem.GetRange(0,Math.Min(rem.Count,nrem.Count))).OrderBy(i=>Guid.NewGuid()).ToList(); // REM-NRのデータ
      data[0]=rem_nrem.Select(x=>x.Spectrum).ToArray();
      ans[0]=rem_nrem.Select(x=>x.PSG==4?1.0:0).ToArray();

      var nrem2=tmp.Where(x=>x.PSG==2).ToList(); // NR2のデータ
      Console.WriteLine($"NR2: {nrem2.Count}");
      var nrem13=tmp.Where(x=>x.PSG==1||x.PSG==3).ToList(); // NR13のデータ
      Console.WriteLine($"NR1,3: {nrem13.Count}");
      var nrem2_nrem13=nrem2.GetRange(0,Math.Min(nrem2.Count,nrem13.Count)).Concat(nrem13.GetRange(0,Math.Min(nrem2.Count,nrem13.Count))).OrderBy(i=>Guid.NewGuid()).ToList(); // NR2-NR13のデータ
      data[1]=nrem2_nrem13.Select(x=>x.Spectrum).ToArray();
      ans[1]=nrem2_nrem13.Select(x=>x.PSG==2?1.0:0).ToArray();

      var nrem1=tmp.Where(x=>x.PSG==3).ToList(); // NR1のデータ
      Console.WriteLine($"NR1: {nrem1.Count}");
      var nrem3=tmp.Where(x=>x.PSG==1).ToList(); // NR3のデータ
      Console.WriteLine($"NR3: {nrem3.Count}");
      var nrem1_nrem3=nrem1.GetRange(0,Math.Min(nrem1.Count,nrem3.Count)).Concat(nrem3.GetRange(0,Math.Min(nrem1.Count,nrem3.Count))).OrderBy(i=>Guid.NewGuid()).ToList(); // NR1-NR3のデータ
      data[2]=nrem1_nrem3.Select(x=>x.Spectrum).ToArray();
      ans[2]=nrem1_nrem3.Select(x=>x.PSG==3?1.0:0).ToArray();

      Console.WriteLine("学習を開始します");
      for(int i=0;i<3;i++){
        Console.WriteLine($"学習中 {i+1}/3");
        var contair=new F64Matrix(data[i].Length,data[i][0].Length);
        Parallel.For(0,data[i].Length,k=>{
          Parallel.For(0,data[i][k].Length,l=>{
            contair.At(k,l,data[i][k][l]);
          });
        });
        try{
          (new ClassificationRandomForestLearner(trees)).Learn(contair,ans[i]).Save(()=>new StreamWriter(output_path+RF_FILE[i]));
        }catch(Exception e){
          Console.WriteLine($"エラー: ランダムフォレストの学習またはモデルの保存に失敗しました\n{e}");
          Environment.Exit(1);
        }
      }
      Console.WriteLine($"全ての学習が終わりました(保存先: {output_path})");
    }

    /// <summary>
    /// 小峯手法のパラメーター群
    /// </summary>
    public class Parameter{
      /// <summary>
      /// 原田手法のトリミングを行うかどうか(先に原田手法の推定が必要) [true]
      /// </summary>
      public bool UseHaradaTrimmingMethod{get;set;}=true;
      /// <summary>
      /// 原田手法のWake判定を用いて結果を置き換えるかどうか(先に原田手法の推定が必要) [true]
      /// </summary>
      public bool ReplaceWakeWithHaradaMethod{get;set;}=true;
    }

  }
}
