﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SleepStageEstimation{
  /// <summary>
  /// 1秒(1つ)のデータ(センサー値と推定値)
  /// </summary>
  public class Record{
    public static readonly string HEADER="date,time,"; // データのヘッダー
    public const int FREQ=16; // Rawデータの周波数
    public DateTime Time{get;private set;} // 時刻
    public int? HR{get;set;}=null; // 心拍
    public int? BM{get;set;}=null; // 体動
    public List<int> SensorValues{get;set;} // Rawデータセンサー値
    public double[] Spectrum{get;set;} // スペクトラムデータ
    public int? PSG{get;set;}=null;
    private Dictionary<string,int?> ss; // 睡眠段階
    public int? this[string key]{
      set{
        ss[key]=value;
      }
      get{
        if(ss.ContainsKey(key))
          return ss[key];
        return null;
      }
    }

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="time">時刻</param>
    /// <param name="hr">心拍</param>
    /// <param name="bm">体動</param>
    public Record(DateTime time,int? hr=null,int? bm=null){
      Time=time;
      HR=hr;
      BM=bm;
      SensorValues=new List<int>();
      Spectrum=null;
      ss=new Dictionary<string,int?>();
    }
    
    /// <summary>
    /// Recordの文字列化
    /// </summary>
    /// <param name="psg">PSGを表示するか</param>
    /// <param name="keys">メソッド名</param>
    /// <returns>文字列変換されたデータ</returns>
    public string ToString(bool psg=false,string[] keys=null){
      var tmp=(keys==null||keys.Length==0)?"":string.Join(",",keys.Select(key=>this[key]==null?"":this[key].ToString()));
      if(psg)
        tmp=(PSG!=null?PSG.ToString():"")+","+tmp;
      return $"{Time.ToString("yyyy/MM/dd,HH:mm:ss")},{tmp}";
    }

    /// <summary>
    /// 16個データがないときに平均値で埋めたリストを返す
    /// </summary>
    /// <returns>16個のデータのリスト</returns>
    public List<int> Get16HzData(){
      if(SensorValues.Count==FREQ)
        return SensorValues;
      else if(SensorValues.Count==0)
        return null;
      var tmp=(int)SensorValues.Average();
      var list=new List<int>(SensorValues);
      for(int i=0;i<FREQ-SensorValues.Count;i++)
        list.Add(tmp);
      return list;
    }

    /// <summary>
    /// クローンする
    /// </summary>
    /// <returns>クローンされたインスタンス</returns>
    public Record Clone(){
      var r=new Record(Time,HR,BM);
      r.ss=new Dictionary<string,int?>(ss);
      r.SensorValues=new List<int>(SensorValues);
      r.Spectrum=Utilities.DeepArrayCopy(Spectrum);
      return r;
    }

  }
}
