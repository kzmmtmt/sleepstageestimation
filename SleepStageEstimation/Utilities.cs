﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SleepStageEstimation{
  /// <summary>
  /// ユーティリティー集
  /// </summary>
  public static class Utilities{

    /// <summary>
    /// 配列のディープコピー(値型のみ)
    /// </summary>
    /// <param name="array">ディープコピーしたい配列</param>
    /// <returns>ディープコピーされた配列</returns>
    public static Type[] DeepArrayCopy<Type>(Type[] array){
      if(array==null)
        return null;
      var tmp=new Type[array.Length];
      Parallel.For(0,array.Length,i=>{
        tmp[i]=array[i];
      });
      return tmp;
    }

    /// <summary>
    /// 文字列の日付と時刻をDateTimeに変換
    /// </summary>
    /// <param name="date">日付(年/月/日 の形式)</param>
    /// <param name="time">時刻(時:分:秒 の形式)</param>
    /// <returns>DateTime</returns>
    public static DateTime? StringToDateTime(string date,string time){
      try{
        var d=(date.Trim().Split('/')).Select(s=>int.Parse(s)).ToList();
        var t=(time.Trim().Split(':')).Select(s=>int.Parse(s)).ToList();
        return new DateTime(d[0],d[1],d[2],t[0],t[1],t[2]);
      }catch(Exception){
        Console.WriteLine("エラー: ファイルの数値が空かデータの形式が正しくありません");
        Environment.Exit(1);
      }
      return null;
    }

    /// <summary>
    /// 秒を「n時間n分n秒」の形式にする
    /// </summary>
    /// <param name="sec">秒</param>
    /// <returns>n時間n分n秒の形式の文字列</returns>
    public static string SecToString(int sec){
      var t=TimeSpan.FromSeconds(sec);
      var str="";
      if(t.Hours>0)
        str+=t.Hours+"時間";
      if(t.Minutes>0)
        str+=t.Minutes+"分";
      if(t.Seconds>0||(t.Minutes==0&&t.Hours==0))
        str+=t.Seconds+"秒";
      return str;
    }

  }
}
