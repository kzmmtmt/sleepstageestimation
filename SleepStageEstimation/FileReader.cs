﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SleepStageEstimation{
  /// <summary>
  /// 各種ファイルの読み込み
  /// </summary>
  public static class FileReader{

    // 各カラムのキャプションの定義
    private const string DATE_COLUMN="Date"; // 日付(YYYY/MM/DD)
    private const string TIME_COLUMN="Time"; // 時間(HH:MM:SS)
    private const string EMFIT_HR_COLUMN="Heart Rate"; // (EMFIT)心拍
    private const string EMFIT_BM_COLUMN="Activity Level(method1 interval 1s)"; // (EMFIT)体動
    private const string TANITA_RAW_SENSOR_COLUMN="sensor1"; // (Tanita-Raw)センサー値
    private const string ALICE_SLEEP_STAGE_COLUMN="SS"; // (ALICE)睡眠段階

    /// <summary>
    /// ファイルからデータの読み込むベース(1行目にキャプションを付けること)
    /// </summary>
    /// <param name="filename">ファイル名</param>
    /// <param name="records">保存先</param>
    /// <param name="act">データの処理</param>
    private static void ReadFileBase(string filename,Records records,Action<Dictionary<string,int>,string[],DateTime> act){
      Console.WriteLine(filename+"を読み込んでいます");
      try{
        using(var sr=new StreamReader(filename)){
          var data=sr.ReadLine().Trim().Split(','); // 1行目(キャプション)を読み込む
          var caption=new Dictionary<string,int>();
          for(int i=0;i<data.Length;i++)
            caption[data[i].Trim()]=i;
          string line;
          while((line=sr.ReadLine())!=null){ // 2行目以降の読み込み
            data=line.Trim().Split(',');
            var dt=(DateTime)Utilities.StringToDateTime(data[caption[DATE_COLUMN]].Trim(),data[caption[TIME_COLUMN]].Trim());
            act(caption,data,dt);
          }
        }
      }catch(KeyNotFoundException){
        Console.WriteLine("エラー: 指定されたキャプションがみつかりません");
        Environment.Exit(1);
      }catch(FormatException){
        Console.WriteLine("エラー: ファイルの数値が空かデータの形式が正しくありません");
        Environment.Exit(1);
      }catch(Exception e){
        Console.WriteLine($"エラー: ファイルの読み込みに失敗しました\n{e.Message}");
        Environment.Exit(1);
      }
      if(records.Count==0){
        Console.WriteLine("エラー: データが空です");
        Environment.Exit(1);
      }else{
        records.ModifyLossAndOverlapping();
        Console.WriteLine($"{records[0].Time.ToString("yyyy/MM/dd-HH:mm:ss")}から{records[records.Count-1].Time.ToString("yyyy/MM/dd-HH:mm:ss")}までの{Utilities.SecToString(records.Count)}のデータを取り込みました");
      }
    }

    /// <summary>
    /// EMFITの1Hzのデータの読み込み(1行目にキャプションを付けること)
    /// </summary>
    /// <param name="filename">ファイル名</param>
    /// <param name="rcds">保存先</param>
    /// <returns>読み込んだファイルをRecordsに変換したデータ</returns>
    public static void ReadEmfitFile(string filename,ref Records rcds){
      var records=new Records();
      ReadFileBase(filename,records,(caption,data,dt)=>{
        if(records.Count==0){
          var r=new Record(dt,int.Parse(data[caption[EMFIT_HR_COLUMN]]),int.Parse(data[caption[EMFIT_BM_COLUMN]]));
          records.Contents.Add(r);
        }else if(records.Contents.Last().Time<dt){
          var r=new Record(dt,int.Parse(data[caption[EMFIT_HR_COLUMN]]),int.Parse(data[caption[EMFIT_BM_COLUMN]]));
          records.Contents.Add(r);
        }else if(records.Contents.First().Time>dt){
          var r=new Record(dt,int.Parse(data[caption[EMFIT_HR_COLUMN]]),int.Parse(data[caption[EMFIT_BM_COLUMN]]));
          records.Contents.Insert(0,r);
        }else{ // もう既にデータが有る
          var index=records.FindIndex(dt);
          records[index].HR=int.Parse(data[caption[EMFIT_HR_COLUMN]]);
          records[index].BM=int.Parse(data[caption[EMFIT_BM_COLUMN]]);
        }
      });
      MergeRecords(records,ref rcds,(to,from)=>{
        to.HR=from.HR;
        to.BM=from.BM;
      });
    }

    /// <summary>
    /// TANITAの16HzのRawデータの読み込み(1行目にキャプションを付けること)
    /// </summary>
    /// <param name="filename">ファイル名</param>
    /// <param name="records">保存先</param>
    /// <returns>読み込んだファイルをRecordsに変換したデータ</returns>
    public static void ReadTanitaRawFile(string filename,ref Records records){
      var data=new Records();
      ReadFileBase(filename,data,(caption,tmp,time)=>{
        if(data.Count!=0){
          if(data.Contents.Last().Time==time)
            data.Contents.Last().SensorValues.Add(int.Parse(tmp[caption[TANITA_RAW_SENSOR_COLUMN]]));
          else{
            if((int)(time-data.Contents.Last().Time).TotalSeconds==1){
              var r=new Record(time);
              r.SensorValues.Add(int.Parse(tmp[caption[TANITA_RAW_SENSOR_COLUMN]]));
              data.Contents.Add(r);
            }else{
              for(int s=1;s<=(int)(time-data.Contents.Last().Time).TotalSeconds;s++){
                var r=new Record(data.Contents.Last().Time.AddSeconds(s));
                r.SensorValues.Add((int)data.Contents.Last().SensorValues.Average());
                data.Contents.Add(r);
              }
            }
          }
        }else{
          var r=new Record(time);
          r.SensorValues.Add(int.Parse(tmp[caption[TANITA_RAW_SENSOR_COLUMN]]));
          data.Contents.Add(r);
        }
      });
      MergeRecords(data,ref records,(to,from)=>{
        to.SensorValues=from.SensorValues;
        to.Spectrum=from.Spectrum;
      });
    }

    /// <summary>
    /// ALICE(PSG)のファイルの読み込み(1行目にDate, Time, SSのキャプションを付けること)
    /// </summary>
    /// <param name="filename">ファイル名</param>
    /// <param name="rcds">保存先</param>
    /// <returns>読み込んだファイルをRecordsに変換したデータ</returns>
    public static void ReadAliceFile(string filename,ref Records rcds){
      var records=new Records();
      ReadFileBase(filename,records,(caption,data,dt)=>{
        if(records.Count==0){
          var r=new Record(dt);
          r.PSG=int.Parse(data[caption[ALICE_SLEEP_STAGE_COLUMN]]);
          records.Contents.Add(r);
        }else if(records.Contents.Last().Time<dt){
          var r=new Record(dt);
          r.PSG=int.Parse(data[caption[ALICE_SLEEP_STAGE_COLUMN]]);
          records.Contents.Add(r);
        }else if(records.Contents.First().Time>dt){
          var r=new Record(dt);
          r.PSG=int.Parse(data[caption[ALICE_SLEEP_STAGE_COLUMN]]);
          records.Contents.Insert(0,r);
        }
      });
      MergeRecords(records,ref rcds,(to,from)=>{
        to.PSG=from.PSG;
      });
      rcds.HasPSG=true;
    }

    /// <summary>
    /// 既に推定されたのデータの読み込み(1行目にキャプションを付けること)
    /// </summary>
    /// <param name="filename">ファイル名</param>
    /// <param name="ss_colmun">睡眠段階カラム名</param>
    /// <param name="method_name">手法名</param>
    /// <param name="rcds">保存先</param>
    /// <returns>読み込んだファイルをRecordsに変換したデータ</returns>
    public static void ReadEstimetedResult(string filename,string ss_column,string method_name,ref Records rcds){
      var records=new Records();
      ReadFileBase(filename,records,(caption,data,dt)=>{
        if(records.Count==0){
          var r=new Record(dt);
          r[method_name]=int.Parse(data[caption[ss_column]]);
          records.Contents.Add(r);
        }else if(records.Contents.Last().Time<dt){
          var r=new Record(dt);
          r[method_name]=int.Parse(data[caption[ss_column]]);
          records.Contents.Add(r);
        }else if(records.Contents.First().Time>dt){
          var r=new Record(dt);
          r[method_name]=int.Parse(data[caption[ss_column]]);
          records.Contents.Insert(0,r);
        }else{ // もう既にデータが有る
          var index=records.FindIndex(dt);
          records[index][method_name]=int.Parse(data[caption[ss_column]]);
        }
      });
      MergeRecords(records,ref rcds,(to,from)=>{
        to[method_name]=from[method_name];
      });
      rcds.AddMethodKey(method_name);
    }

    /// <summary>
    /// fromからtoにRecordsをマージする
    /// </summary>
    /// <param name="from">マージ元</param>
    /// <param name="to">マージ先</param>
    /// <param name="act">処理</param>
    private static void MergeRecords(Records from,ref Records to,Action<Record,Record> act){
      from.ModifyLossAndOverlapping();
      if(to.Count==0)
        to=from;
      else{
        var t1=(int)(to.Contents.First().Time-from.Contents.First().Time).TotalSeconds;
        if(t1>0){ // fromの方が先
          for(int t=t1-1;t>=0;t--){
            to.Contents.Insert(0,from[t]);
            if(t==from.Count-1)
              return;
          }
          for(int t=0;t<to.Count;t++){
            act(to[t],from[t+t1]);
            if(t+t1==from.Count-1)
              return;
          }
          for(int t=t1+to.Count;t<from.Count;t++)
            to.Contents.Add(from[t]);
        }else{ // toが先
          for(int t=0;t<to.Count;t++){
            act(to[t-t1],from[t]);
            if(t==from.Count-1||t-t1>to.Count-1)
              return;
          }
          for(int t=to.Count+t1;t<from.Count;t++)
            to.Contents.Add(from[t]);
        }
      }
      to.ModifyLossAndOverlapping();
    }

  }
}
