﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Statistics;

namespace SleepStageEstimation{
  /// <summary>
  /// 上原手法
  /// </summary>
  public static class UeharaMethod{
    public const string NAME="Uehara";

    /// <summary>
    /// 睡眠段階の推定(原田手法と小峯手法の結果が必要)
    /// </summary>
    /// <param name="records">データ(既に原田手法と小峯手法の結果が入ったもの)</param>
    /// <param name="param">パラメーター群</param>
    public static void EstimateSleepStage(Records records,Parameter param){
      Console.WriteLine("上原手法で推定しています");
      if(!(records.HasMethodKey(HaradaMethod.NAME)&&records.HasMethodKey(KomineMethod.NAME))){
        Console.WriteLine("エラー: 上原手法の実行には事前に原田手法と小峯手法を適用している必要があります");
        Environment.Exit(1);
      }
      var flag=RaiseFlag(records,param);
      var ss=ExtractN1W(flag,param);
      var d=records.Contents.Where(x=>x.SensorValues.Count!=0);
      var start=records.FindIndex(d.First().Time);
      ExtractR(ref ss,records.Contents.ToArray(),start,param);
      ExtractN3(ref ss,param);

      // 出力
      records.AddMethodKey(NAME);
      var end=records.FindIndex(d.Last().Time);
      Parallel.For(start+param.SDTime,end,i=>{
        if(records[i][HaradaMethod.NAME]!=null)
          records[i][NAME]=ss[i-(start+param.SDTime)]==null?2:ss[i-(start+param.SDTime)];
      });
    }

    /// <summary>
    /// SDが一定上のところにフラグを立てる
    /// </summary>
    /// <param name="records">データ</param>
    /// <param name="param">パラメーター</param>
    /// <returns>フラグ(SDTime～最後まで)</returns>
    private static bool[] RaiseFlag(Records records,Parameter param){
      Console.WriteLine("標準偏差を計算しています");
      var data=records.Contents.Where(x=>x.SensorValues.Count!=0).ToArray();
      var flag=new bool[data.Length-param.SDTime];
      Parallel.For(param.SDTime,data.Length,i=>{
        var avr=new double[param.SDTime];
        for(int j=0;j<param.SDTime;j++)
          avr[j]=data[i-j].SensorValues.Select(x=>(double)x).Average();
        flag[i-param.SDTime]=avr.StandardDeviation()>param.Threshold;
      });
      return flag;
    }

    /// <summary>
    /// 後ろのN1WTime秒をNR1、それ以外をWに
    /// </summary>
    /// <param name="flag"></param>
    /// <param name="param"></param>
    /// <returns>睡眠段階</returns>
    private static int?[] ExtractN1W(bool[] flag,Parameter param){
      Console.WriteLine("Wake-NR1を抽出しています");
      var ss=new int?[flag.Length];
      Parallel.For(0,ss.Length,i=>{
        ss[i]=flag[i]?5:(int?)null;
      });
      // flagが止んでN1WTime以上続いていたら
      for(int i=param.N1WTime;i<flag.Length;i++)
        if(((!flag[i]&&flag[i-1])||(i==flag.Length-1&&flag[i]))&&FlagConituesLongerThan(param.N1WTime,i-1,ss,5))
          for(int j=0;j<param.N1WTime;j++)
            ss[i-1-j]=3;
      return ss;
    }

    /// <summary>
    /// 一定時間targetの状態が続いているかどうかを調べる
    /// </summary>
    /// <param name="t">一定時間</param>
    /// <param name="now">基準時間</param>
    /// <param name="ss">睡眠段階</param>
    /// <param name="target">target</param>
    /// <returns>結果</returns>
    private static bool FlagConituesLongerThan(int t,int now,int?[] ss,int target){
      for(int i=1;i<t;i++){
        if(ss[now-i]!=target)
          return false;
      }
      return true;
    }

    /// <summary>
    /// REMの抽出
    /// </summary>
    /// <param name="ss">睡眠段階</param>
    /// <param name="data">データ</param>
    /// <param name="start">flagの立ち初めのrecordのindex</param>
    /// <param name="param">パラメーター群</param>
    private static void ExtractR(ref int?[] ss,Record[] data,int start,Parameter param){
      Console.WriteLine("REMを抽出しています");
      for(int i=0;i<ss.Length;i++){
        // 原田手法がREMフラグならば無条件にREM
        if(data[i+start+param.SDTime][HaradaMethod.NAME]==4){
          ss[i]=4;
          // さらに小峯手法もREMならば近くのREMをつなげる(但しWは上書きしない)          
          if(data[i+start+param.SDTime][KomineMethod.NAME]==4){
            bool flag1=false,flag2=false;
            for(int t=param.KomineRemInterval;t>0;t--){
              if(i+start+param.SDTime-t>0&&!flag1)
                flag1=data[i+start+param.SDTime-t][KomineMethod.NAME]==4;
              else
                flag1=false;
              if(i+start+param.SDTime+t<data.Length&&!flag2)
                flag2=data[i+start+param.SDTime+t][KomineMethod.NAME]==4;
              else
                flag2=false;
              if(flag1)
                if(ss[i-t]!=5)
                  ss[i-t]=4;
              if(flag2)
                if(ss[i+t]!=5)
                  ss[i+t]=4;
            }
          }
        }
      }
    }

    /// <summary>
    /// REMの後ろのN3Time秒をNR3に
    /// </summary>
    /// <param name="ss">睡眠段階</param>
    /// <param name="param">パラメーター群</param>
    /// <returns>睡眠段階</returns>
    private static void ExtractN3(ref int?[] ss,Parameter param){
      Console.WriteLine("NR3を抽出しています");
      // flagが止んでN3RTime以上続いていたら
      for(int i=param.N3RTime;i<ss.Length;i++)
        if(((ss[i]!=4&&ss[i-1]==4)||(i==ss.Length-1&&ss[i]==4))&&FlagConituesLongerThan(param.N3RTime,i-1,ss,4))
          for(int j=0;j<param.N3RTime;j++)
            ss[i-1-j]=1;
    }


    /// <summary>
    /// パラメーター群
    /// </summary>
    public class Parameter{
      /// <summary>
      /// 閾値 [200]
      /// </summary>
      public int Threshold{get;set;}=200;
      /// <summary>
      /// N1WTime秒をN1, それより前をW [30]
      /// </summary>
      public int N1WTime{get;set;}=30;
      /// <summary>
      /// 後ろN3RTIME秒をN3, それより前をR [30]
      /// </summary>
      public int N3RTime{get;set;}=30;
      /// <summary>
      /// センサー値の何秒間の標準偏差を取るか [30]
      /// </summary>
      public int SDTime{get;set;}=30;
      /// <summary>
      /// 小峯手法において連続的にREMと判断する基準の時間
      /// </summary>
      public int KomineRemInterval{get;set;}=120;
    }
  }
}
